Welcome to the open-liconic-stx documentation!
==============================================

.. toctree::
   :maxdepth: 2

This module is an unofficial Python interface for LiCONiC STX devices.

Usage
-----

To control the STX, use the :py:class:`stx.Stx` class.
It is strongly recommended to use the Stx class as a context manager, else the connection has to be :py:func:`opened <stx.Stx.open_connection>` and :py:func:`closed <stx.Stx.close_connection>` manually:

.. code-block::

    >>> from stx import Stx

    >>> with Stx("COM3") as s:
    ...    print("Current temperature:", s.climate_controller.current_temperature)
    ...    print("Plate on transfer station?", s.plate_handler.initialize())
    ...    s.plate_handler.move_plate_from_transfer_station_to_slot(stacker=1, slot=5)
    ...
    Current temperature: 37.3
    Plate on transfer station? True

The Stx class
-------------

.. autoclass:: stx.Stx

Components
----------

.. autoclass:: stx.climate_controller.ClimateController

.. autoclass:: stx.plate_handler.PlateHandler

.. autoclass:: stx.shaker_controller.ShakerController

Memory Map
----------

.. autoclass:: stx.memory_map.MemoryMap

Serial Port
-----------

.. autoclass:: stx.serial_port.SerialPort

Errors
------

.. automodule:: stx.errors
    :exclude-members: with_traceback
